![N|Solid](https://i.ibb.co/WyHv7SB/terra.jpg)

![Pipeline](https://gitlab.com/infoport/infrastructure/badges/master/pipeline.svg)

**ECSkeleton** enables projects to be cloud-hosted, self-deployable (both infrastructure and application) and tested in a fully automated manner. 

The main idea behind this project is to set the infrastructure skeleton for any project who uses a unique docker image as an artifact. 

# Features
  - Automatic deployments of infrastructure changes
  - Automatic deployment of application changes
  - Application agnostic
  - Fully extensible and customizable

# Tools
  - Terraform
  - Docker
  - Docker-compose
  - GitLab CI

# Flowchart

```mermaid
graph TD;
A[Application pipeline]
B[ECSkeleton pipeline triggered]
C[Gitlab runs Terraform scripts]
D[AWS Provisions]
E[VPC]
F[Subnets]
G[SGs]
H[ECS Cluster]
I[iGW]
J[ALB]
K[EIP]
L[Fargate task]
  A -- Docker Image -->B
  B --> C
  C --> D
  D --> E
  D --> F
  D --> G
  D --> H
  D --> I
  D --> J
  D --> K
  H --> L
```

# Setup & configuration

First of all you need to create a fork of this repository per application.

For instance, the "trocola" application will need a fork of this repository called "trocola_infrastructure" with these pre-configurations:

#### Gitlab CI/CD Variables

Once forked you will need to create these CI/CD variables on the gitlab forked project:

**terraform_backend** 

This is a base64 encoded Terraform backend configuration file.

Unencoded example:
```
terraform {
 backend "s3" {
 encrypt = true
 bucket = "example-remote-state-storage-s3"
 dynamodb_table = "terraform-state-lock-dynamo"
 region = "eu-west-1"
 key = "state/terraform.tfstate"
 access_key = "XXXXXXXXXXXXXXXXXXXXXXXXXXX"
 secret_key = "XXXXXXXXXXXXXXXXXXXXXXXXXXX"
 }
}
```

Encode it with ``cat filename | base64`` on any linux flavor and then save the base64 output string in the variable value.

**terraform_creds** 

This is a base64 encoded Terraform .tfvars configuration file wich contains AWS auth creds to deploy resources.

Unencoded example:
```
aws_access_key = "XXXXXXXXXXXXXXXXXXXXXXXXXXX"
aws_secret_key = "XXXXXXXXXXXXXXXXXXXXXXXXXXX"
```

Encode it with ``cat filename | base64`` on any linux flavor and then save the base64 output string in the variable value.

#### Pipeline triggers

You will need to setup a pipeline trigger in the gitlab forked project to be able to trigger the CI/CD process, i will reference it in the examples as ``INFRATRIGGER``.

#### Project ID

In order to trigger this project (The forked one) please write down the project ID because you'll need it in the next step.

#### Trigger application deployment on CI/CD

To use this repository you will need to push a Docker Image to a registry and then trigger CD/CI process of this repository providing the image name as variable.

Trigger example:

**NOTE:** Replace ``INFRATRIGGER``, ``DOCKERIMAGE`` & ``THISPROJECTID``:

```
curl --request POST --form "token=INFRATRIGGER" --form "ref=master" --form "variables[APP_IMAGE]=DOCKERIMAGE https://gitlab.com/api/v4/projects/THISPROJECTID/trigger/pipeline
```

#### Terraform backend bucket and DynamoDB creation

To enable Terraform state lock you'll need to provision these resources only ONCE:

```
resource "aws_s3_bucket" "terraform-state-storage-s3" {
    bucket = "example-remote-state-storage-s3"

    versioning {
      enabled = true
    }

    lifecycle {
      prevent_destroy = true
    }

    tags {
      Name = "S3 Remote Terraform State Store"
    }
}

resource "aws_dynamodb_table" "dynamodb-terraform-state-lock" {
  name = "terraform-state-lock-dynamo"
  hash_key = "LockID"
  read_capacity = 20
  write_capacity = 20

  attribute {
    name = "LockID"
    type = "S"
  }

  tags {
    Name = "DynamoDB Terraform State Lock Table"
  }
}
```

#### Customize the deployment with your application needs

You can customize some aspects of the deployment such as container port, instance size, etc. in the [variables.tf ](aws/variables.tf)file.

**Once you're ready to go, just trigger the pipeline and chill.**

#### Debug and run locally with compose

In order to provision infrastructure without locally installing terraform, use ``` docker-compose run terraform <command> ``` in the project's root directory.
