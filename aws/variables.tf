variable "aws_access_key" {}

variable "aws_secret_key" {}

variable "aws_region" {
    description = "AWS region."
    default     = "eu-west-1"
}
variable "az_count" {
  description = "Number of AZs to cover in a given AWS region"
  default     = "2"
}

variable "vpc_cidr" {
    description = "VPC's CIDR"
    default     = "10.0.0.0/16"
}

variable "environment" {
    description = "Environment tag"
    default     = "staging"
}
variable "app_image" {
  description = "Docker image to run in the ECS cluster"
  default     = "adongy/hostname-docker:latest"
}

variable "app_port" {
  description = "Port exposed by the docker image to redirect traffic to"
  default     = 3000
}

variable "lb_port" {
  description = "Port exposed by the ALB to internet"
  default     = 80
}

variable "app_count" {
  description = "Number of docker containers to run"
  default     = 2
}

variable "fargate_cpu" {
  description = "Fargate instance CPU units to provision (1 vCPU = 1024 CPU units)"
  default     = "256"
}

variable "fargate_memory" {
  description = "Fargate instance memory to provision (in MiB)"
  default     = "512"
}
